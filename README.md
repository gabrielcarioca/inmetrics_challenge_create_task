# Desafio QA

Esse projeto é uma submissão do desafio QA para a Inmetrics
(PAra referência, desenvovli e rodei os tests em OSX)

## Pre requisitos

Abaixo uma lista dos pré requisitos e como instalá-los

### Selenium

```
npm install -g selenium-server
npm install -g selenium-standalone
```

### Chromedriver

Tenha certeza que a última versão do Google Chrome está instalada no seu computador.

```
npm install -g chromedriver
```

### Gradle
```
brew install gradle
```

## Executando os testes

### Rodando o Selenium

```
selenium-standalone start -- -role hub
```

### Executando testes para criar task no site juliodelima

Do repositório do projeto

```
gradle test -Dcucumber.options="-t @SignUpFeature"

ou

./gradlew test -Dcucumber.options="-t @SignUpFeature"
```

### Executando testes de Web Service da lista de filmes

Do repositório do projeto

```
gradle test -Dcucumber.options="-t @GetFilmsList"

ou

./gradlew test -Dcucumber.options="-t @GetFilmsList"
```

A lista de filmes desejada será impressa no terminal