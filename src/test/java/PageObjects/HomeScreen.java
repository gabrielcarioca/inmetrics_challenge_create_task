package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomeScreen extends AbstractScreen {

    @FindBy(id = "signup")
    private WebElement signUpButton;

    @FindBy(xpath="//a[contains(text(), 'add some tasks!')]")
    private WebElement addTaskButton;

    public HomeScreen(WebDriver driver) {
        super(driver);
    }

    public WebElement getSignUpButton() {
        return signUpButton;
    }

    public WebElement getAddTaskButton() {
        return addTaskButton;
    }

    public void waitForSignUpButton() {
        waitUtility().waitForElementToBeClickable(driver, signUpButton);
    }

    public void waitForAddTaskButton() {
        waitUtility().waitForElementToBeClickable(driver, addTaskButton);
    }

}
