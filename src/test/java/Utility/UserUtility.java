package Utility;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.RandomStringUtils;

public class UserUtility {
    // Variables to use in login when necessary
    private static String login;
    private static String name;
    private static String password;

    // Singleton
    private static UserUtility singleInstance;

    public static UserUtility getInstance(){
        if(singleInstance == null){
            singleInstance = new UserUtility();
        }
        return singleInstance;
    }

    private Faker faker;

    private boolean isFakerStarted = false;

    public void startANewFaker() {

        if (!isFakerStarted) {
            // create a new instance of the Faker library
            Faker faker = new Faker();

            this.faker = faker;
            isFakerStarted = true;
        }
    }

    public void setLogin(String login) {
        UserUtility.login = login;
    }

    public void setName(String name) {
        UserUtility.name = name;
    }

    public void setPassword (String password) {
        UserUtility.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public Faker getFaker() {
        return faker;
    }

    public String generateRandomName() {
        String firstName = faker.name().firstName();
        UserUtility.getInstance().setName(firstName);
        return firstName;
    }

    public String generateRandomLogin() {
        String login = faker.name().lastName();
        UserUtility.getInstance().setLogin(login);
        return login;
    }

    public String generateRandomPassword() {
        String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lower = "abcdefghijklmnopqrstuvwxyz";
        String digits = "1234567890";
        String character = "!@#$%^&*-_=+|;:,<.>/?";
        String password = RandomStringUtils.random( 15, upper+lower+digits+character);
        UserUtility.getInstance().setPassword(password);
        return password;
    }
}
