package Stepdefs;

import PageObjects.*;
import Utility.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.Keys;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

public class CreateTaskStepDefs extends BaseStepDefs{
    public HomeScreen homeScreen;
    public SignUpModalScreen signUpModalScreen;
    public TasksScreen tasksScreen;
    public AddTaskModalScreen addTaskModalScreen;

    private Task lastCreatedTask;

    @Before(order = 1)
    public void beforeSuite() {
        System.out.println("BeforeTest");

        homeScreen = new HomeScreen(driver());
        signUpModalScreen = new SignUpModalScreen(driver());
        tasksScreen = new TasksScreen(driver());
        addTaskModalScreen = new AddTaskModalScreen(driver());

        lastCreatedTask = new Task();
    }

    @Given("^User goes to home page$")
    public void goToHomePage() throws MalformedURLException{
        driver().navigate().to(new URL("http://www.juliodelima.com.br/taskit/"));
        homeScreen.waitForSignUpButton();
    }

    @When("^User registers into the system$")
    public void signUpIntoSystem() {
        homeScreen.getSignUpButton().click();
        signUpModalScreen.waitForSignUpModal();
        signUpModalScreen.getNameField().sendKeys(UserUtility.getInstance().generateRandomName());
        signUpModalScreen.getLoginField().sendKeys(UserUtility.getInstance().generateRandomLogin());
        signUpModalScreen.getPasswordField().sendKeys(UserUtility.getInstance().generateRandomPassword());
        signUpModalScreen.getSaveButton().click();
        homeScreen.waitForAddTaskButton();
    }

    @Then("^User navigates to tasks screen$")
    public void goToTasksScreen() {
        homeScreen.getAddTaskButton().click();
        tasksScreen.waitForTasksScreenToLoad();
    }

    @And("^User clicks to create a new task$")
    public void openCreateNewTaskModal() {
        tasksScreen.getAddATaskButton().click();
        addTaskModalScreen.waitForPageToLoad();
    }

    @And("^User fills task title with a random text$")
    public void fillTaskTitleWithRandomValue() {
        String taskTitle = UserUtility.getInstance().getFaker().pokemon().name();
        addTaskModalScreen.getTitleField().sendKeys(taskTitle);

        lastCreatedTask.setTaskName(taskTitle);
    }

    @And("^User sets date limit to today$")
    public void selectDateLimitAsToday() {
        // Opening date picker for date limit field
        // For some reason the date picker is not opening in my chrome browser and I didn't have the time to fully investigate
        // So I had to do this workaround to click on title field and press tab so the modal wouldn't automatically close
        addTaskModalScreen.getTitleField().click();
        String tab = Keys.chord(Keys.TAB);
        addTaskModalScreen.getTitleField().sendKeys(tab);

        addTaskModalScreen.getDateLimitTodayButton().click();
        addTaskModalScreen.getDateLimitOkButton().click();
        WaitUtility.getInstance().waitForElementToBeInvisible(driver(), addTaskModalScreen.getDateLimitOkButton());
        // Validating if the correct date is appearing in the field after selection
        addTaskModalScreen.dateLimitValueShouldBeToday();

        lastCreatedTask.setTaskTimeLimit(new Date());
    }

    @And("^User sets time limit as (\\d+):(\\d+)$")
    public void setTaskTimeLimit(String hour, String minutes) {
        // Opening time picker for time limit field
        // For some reason the time picker is not opening in my chrome browser and I didn't have the time to fully investigate
        // So I had to do this workaround to click on tell us more field and press shift+tab
        addTaskModalScreen.getTellUsMoreField().click();
        String shift_tab = Keys.chord(Keys.SHIFT, Keys.TAB);
        addTaskModalScreen.getTellUsMoreField().sendKeys(shift_tab);
        // Selecting hour and minute in time picker
        addTaskModalScreen.selectHourInTimeLimitPicker(hour);
        addTaskModalScreen.selectMinutesInTimeLimitPicker(minutes);
        addTaskModalScreen.getTimeLimitOkButton().click();
        WaitUtility.getInstance().waitForElementToBeInvisible(driver(), addTaskModalScreen.getDateLimitOkButton());
        // Validating if the correct time option is appearing in the field after selection
        addTaskModalScreen.timeLimitValueShouldBe(hour+":"+minutes);
        addTaskModalScreen.timeLimitValueShouldBe(hour+":"+minutes);

        Calendar calendar=Calendar.getInstance();
        calendar.setTime(lastCreatedTask.getTaskTimeLimit());
        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
        calendar.set(Calendar.MINUTE, Integer.parseInt(minutes));
        calendar.set(Calendar.SECOND,0);
        Date taskTimeLimit=calendar.getTime();

        lastCreatedTask.setTaskTimeLimit(taskTimeLimit);
    }

    @And("^User fills task note as \"([^\"]*)\"$")
    public void setTaskNote(String note) {
        addTaskModalScreen.getTellUsMoreField().sendKeys(note);

        lastCreatedTask.setTaskNote(note);
    }

    @And("^User saves the task$")
    public void saveNewTask() {
        addTaskModalScreen.getSaveButton().click();
        waitUtility().waitForElementToBeInvisible(driver(), addTaskModalScreen.getSaveButton());
    }

    @Then("^User can see the created task$")
    public void validateCreatedTask() {
        try {
            Task lastTask = tasksScreen.getLastTask();
            Assert.assertTrue(lastTask.equals(lastCreatedTask));
        }
        catch (Exception e) {
            Assert.assertTrue(false);
        }
    }
}
