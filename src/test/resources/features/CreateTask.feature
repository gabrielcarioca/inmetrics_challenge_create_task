@SignUpFeature
Feature: Sign Up

Background:
    Given User goes to home page

Scenario: Validate if user is able to register into the system and create a task
    When User registers into the system
    And User navigates to tasks screen
    And User clicks to create a new task
    And User fills task title with a random text
    And User sets date limit to today
    And User sets time limit as 18:20
    And User fills task note as "Remember to test this task"
    And User saves the task
    Then User can see the created task
