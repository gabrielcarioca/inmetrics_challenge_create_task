@GetFilmsList
Feature: Get Films List

Scenario: Get list of films from George Lucas and Rick McCallum
    Given User sends request to find films with director "George Lucas" and producer "Rick McCallum"
    And User receives a 200 response
    Then User can see the list of requested films

